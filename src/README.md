# clientUploadRenderings v0

The /prototype directory contains a functioning prototyping version of the tool clientUploadRenderings. The tool had been used to analyse an upload's problem that we had in the communication between a python's client script, used in a thirdy-part software, and the API owned by us.

The /src directory contains a redesign of the prototype, redesign that try to apply the principle of the Atomic Design to a "nonweb" application.

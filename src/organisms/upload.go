package organisms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/golele/clientuploadrenderings/src/atoms"
	"gitlab.com/golele/clientuploadrenderings/src/molecules"
)

type uploadRequest struct {
	url         string
	body        molecules.PUTBody
	renderingID string
}

func getPUTURL(config molecules.ConfigOptions, collectionID, renderingID string) string {
	return fmt.Sprintf("%s%s/renderings/%s", config.BaseURL, collectionID, renderingID)
}

func addHeaderRequest(req *http.Request, config molecules.ConfigOptions) {
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Basic "+config.BasicAuth)
}

func (u *uploadRequest) doUpload(config molecules.ConfigOptions) error {
	body, err := json.Marshal(&u.body)
	if err != nil {
		return err
	}
	req, err := http.NewRequest(http.MethodPut, u.url, bytes.NewBuffer(body))
	addHeaderRequest(req, config)
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Failed to Upload The Rendering %s, statusCode = %d", u.renderingID, resp.StatusCode)
	}
	log.Printf("Photo %s successfully uploaded", u.renderingID)
	return nil
}

// Run runs the upload process
func Run() error {
	config := molecules.ConfigOptions{}
	if err := config.Load(); err != nil {
		return err
	}
	if err := config.Check(); err != nil {
		return err
	}
	rDir := config.RenderedPhotoDirectory
	// read input directory
	files, err := ioutil.ReadDir(rDir)
	if err != nil {
		return err
	}
	cdata := molecules.CollectionData{}
	for _, l1 := range files {
		collectionID, _ := strconv.Atoi(l1.Name())
		if err := cdata.Load(config, atoms.CollectionID(collectionID)); err != nil {
			return err
		}
		cDir := rDir + "/" + l1.Name()
		collectionDirectory, err := ioutil.ReadDir(cDir)
		if err != nil {
			return err
		}
		for _, l2 := range collectionDirectory {
			body := molecules.PUTBody{
				Assignments: make(map[int]int),
			}
			body.SetLayoutID(l2.Name())
			lDir := cDir + "/" + l2.Name()
			layoutDirectory, err := ioutil.ReadDir(lDir)
			if err != nil {
				return err
			}
			for _, l3 := range layoutDirectory {
				cameraDir := lDir + "/" + l3.Name()
				cameraName := l3.Name()
				body.SetCameraName(cameraName)
				photos, err := ioutil.ReadDir(cameraDir)
				if err != nil {
					return err
				}
				for _, photo := range photos {
					photoName := photo.Name()
					if err := body.SetImage(cameraDir + "/" + photoName); err != nil {
						log.Fatal(err)
						log.Printf("Could not load the photo %s", cameraDir+"/"+photoName)
					}
					body.SetAssignments(cdata.Data, photoName)
					request := uploadRequest{
						renderingID: photoName,
						url:         getPUTURL(config, string(collectionID), photoName),
						body:        body,
					}
					request.doUpload(config)
				}
			}
		}
	}
	return nil
}

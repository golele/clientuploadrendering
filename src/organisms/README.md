 Organisms are assemblies of molecules functioning together as a unit. These relatively complex structures can range from single-celled organisms all the way up to incredibly sophisticated organisms like human beings.

Let’s revisit our search form molecule. A search form can often be found in the header of many web experiences, so let’s put that search form molecule into the context of a header organism.

 Organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms. These organisms form distinct sections of an interface.

 # Organisms
 
 The header forms a standalone section of an interface, even though it contains several smaller pieces of interface with their own unique properties and functionality.

While some organisms might consist of different types of molecules, other organisms might consist of the same molecule repeated over and over again. For instance, visit a category page of almost any e-commerce website and you’ll see a listing of products displayed in some form of grid.

 Building up from molecules to more elaborate organisms provides designers and developers with an important sense of context.

Organisms demonstrate those smaller, simpler components in action and serve as distinct patterns that can be used again and again.

Now that we have organisms defined in our design system, we can break our chemistry analogy and apply all these components to something that resembles a web page!

## List Of Organismus Presents In ClientUploadRenderings

- collection, is the element that contains more information about one bed. We must GET the whole information through the API interface, but we need only the Symbols

- API request

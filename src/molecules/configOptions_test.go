package molecules

import (
	"errors"
	"testing"

	"gitlab.com/golele/clientuploadrenderings/src/atoms"
)

func TestConfigFileIsReadable(t *testing.T) {
	// Given: the command line has the -c flag set
	// 	 And: the name of the configFile is "hrheiuwre"
	// When: the configFile is NOT found
	atoms.ConfigFile = "hrheiuwre"
	expected := errors.New("open /config/hrheiuwre.json: no such file or directory")
	opts := &ConfigOptions{}
	if got := opts.Load(); got.Error() != expected.Error() {
		// Then: the program must terminate with the error: "no such file or directory"
		t.Errorf("got: %s, expected %s", got, expected)
	}
}

func TestConfigDoesNotHaveBasicAuth(t *testing.T) {
	// Given: basic_auth is NOT present in the config file
	atoms.WorkingDir = "/home/lele/gitlab.com/golele/clientuploadrenderings"
	atoms.ConfigFile = "testEmptyConfig"
	// When: the clientUploadRenderings starts
	expected := errors.New("basic_auth is not defined in the config file")
	opts := &ConfigOptions{}
	if got := opts.Check(); got.Error() != expected.Error() {
		// Then: it must terminate with the error: "No basic_auth found"
		t.Errorf("got: %s, expected %s", got, expected)
	}
}

func TestConfigDoesNotHaveBaseURL(t *testing.T) {
	// Given: base_url is NOT present in the config file
	// 	 And: basic_auth is present
	atoms.WorkingDir = "/home/lele/gitlab.com/golele/clientuploadrenderings"
	atoms.ConfigFile = "testEmptyConfig"
	// When: the clientUploadRenderings starts
	expected := errors.New("base_url is not defined in the config file")
	opts := &ConfigOptions{
		BasicAuth: "dsfsd",
	}
	if got := opts.Check(); got.Error() != expected.Error() {
		// Then: it must terminate with the error: "No basic_auth found"
		t.Errorf("got: %s, expected %s", got, expected)
	}
}

func TestConfigContainsWrongPhotoDirectory(t *testing.T) {
	// Given: the config file contains a not existent photo directory
	atoms.WorkingDir = "/home/lele/gitlab.com/golele/clientuploadrenderings"
	atoms.ConfigFile = "testNotExistentRenderedPhotoDirectory"
	// When: the clientUploadRenderings starts
	msg := "The directory of the rendered photo (/home/lele/gitlab.com/golele/clientuploadrenderings/ioqwrenderings) does not exists"
	expected := errors.New(msg)
	opts := &ConfigOptions{}
	opts.Load()
	if got := opts.Check(); got.Error() != expected.Error() {
		// Then: it must terminate with the error: "The directory of the rendered photo does not exists"
		t.Errorf("got: %s, expected %s", got, expected)
	}
}

func TestConfigDoesNOTContainPhotoDirectory(t *testing.T) {
	// Given: the config file does NOT contain the photo_directory_path
	atoms.WorkingDir = "/home/lele/gitlab.com/golele/clientuploadrenderings"
	atoms.ConfigFile = "testUndefinedRenderedPhotoDirectory"
	// When: the config.Check() will be executed
	expected := errors.New("rendered_photo_path is not defined in the config file")
	opts := &ConfigOptions{}
	opts.Load()
	if got := opts.Check(); got.Error() != expected.Error() {
		// Then: it must terminate with the error: "The directory of the rendered photo does not exists"
		t.Errorf("got: %s, expected %s", got, expected)
	}
}

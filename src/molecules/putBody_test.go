package molecules

import (
	"errors"
	"io/ioutil"
	"reflect"
	"testing"

	"gitlab.com/golele/clientuploadrenderings/src/atoms"
)

func TestSetAssignments(t *testing.T) {
	// Given: renderingID is empty
	// When: the SetAssignment have been called
	// Then: it should return the error "renderingID is required, but it is empty"
	filename := ""
	jdata := Jdata{}
	b := PUTBody{
		Assignments: make(map[int]int),
	}
	t.Run("SetAssignments with empty renderingID", func(t *testing.T) {
		expected := errors.New("renderingID is required, but it is empty")
		if err := b.SetAssignments(jdata, filename); err == nil {
			t.Errorf("SetAssignment got %v, expected %v", err, expected)

		}
	})

	// Given: renderingID is NOT empty
	//   And: the assignment of a collection have been loaded
	// When: for every SymbolID there is an item in the renderingID
	// Then: the length of the assignment in the putBody should have the same length of the Symbols
	// as in this example, we have
	// 9 items:
	// 5323142,
	// 6526537,
	// 5342365,
	// 5296108,
	// 5332192,
	// 5382866,
	// 6302814,
	// 6302814,
	// 6131346
	// and 9 Symbols (1, 2, 4, 5, 8, 9, 12, 13 and 14 )
	t.Run("SetAssignments with known renderingID and loaded collection assignments", func(t *testing.T) {
		filename = "5323142_6526537_5342365_5296108_5332192_5382866_6302814_6302814_6131346"
		jdata = getTestJdataStruct(21)
		expected := atoms.UploadAssignments{
			1:  5323142,
			2:  6526537,
			4:  5342365,
			5:  5296108,
			8:  5332192,
			9:  5382866,
			12: 6302814,
			13: 6302814,
			14: 6131346,
		}
		b.SetAssignments(jdata, filename)
		if !reflect.DeepEqual(b.Assignments, expected) {
			t.Errorf("SetAssignments got %v, expected %v", b.Assignments, expected)
		}
	})
	// Given: renderingID is NOT empty
	//   And: the assignment of a collection have been loaded
	// When: for some Symbols there is NOT an item in the renderingID
	// Then: the length of the assignment in the putBody should have the same length of the items
	// as in this example, we have
	// 6 items:
	// 5323142,
	// 6526537,
	// 5342365,
	// 5296108,
	// 6302814,
	// 6131346
	// and 9 Symbols (1, 2, 4, 5, 8, 9, 12, 13 and 14 )
	t.Run("SetAssignments with known renderingID and loaded collection assignments", func(t *testing.T) {
		filename = "5323142_6526537_5342365_5296108_6302814_6131346"
		jdata = getTestJdataStruct(21)
		expected := atoms.UploadAssignments{
			1:  5323142,
			2:  6526537,
			4:  5342365,
			5:  5296108,
			13: 6302814,
			14: 6131346,
		}
		b.SetAssignments(jdata, filename)
		if !reflect.DeepEqual(b.Assignments, expected) {
			t.Errorf("SetAssignments got %v, expected %v", b.Assignments, expected)
		}
	})
}

func TestSetImage(t *testing.T) {
	// Given: filename in NOT found
	// Wehn: SetImage have been called
	// Then: it should return "no such file or directory"
	filename := ""
	b := PUTBody{}
	t.Run("Filname is NOT found", func(t *testing.T) {
		expected := errors.New("open : no such file or directory")
		got := b.SetImage(filename)
		if got.Error() != expected.Error() {
			t.Errorf("SetImage expected %s, but got %s", expected.Error(), got.Error())
		}
	})

	// Given: filename is found
	// When: SetImage have been called
	// Then: is should set the Image of the PUTBody
	filename = "./fixtures/5323142_6526537_5342365_5296108_5332192_5382866_6302814_6302814_6131346.0001.png"
	t.Run("Filename is found", func(t *testing.T) {
		b.SetImage(filename)
		expected, _ := ioutil.ReadFile("./fixtures/encodedPhoto.txt")
		if string(expected) != b.Image {
			t.Errorf("The photo have not been ancoded correctly")
		}
	})
}

func TestSetLayoutID(t *testing.T) {
	// Given: SetLayoutID have been called with parameter "4"
	// Then: the PUTBody.LayoutID should be "4"
	b := PUTBody{}
	t.Run("SetLayoutID with id 4", func(t *testing.T) {
		b.SetLayoutID("4")
		expected := atoms.LayoutID(4)
		if expected != b.LayoutID {
			t.Errorf("SetLayouID set the wrong LayoutID, set %v expected %v", b.LayoutID, expected)
		}
	})
}

func TestCameraName(t *testing.T) {
	// Given: SetCameraName have been called with parameter "birdeyes"
	// Then: the PUTBody.CameraName should be "birdeyes"
	b := PUTBody{}
	t.Run("SetCameraName with birdeyes as parameter", func(t *testing.T) {
		b.SetCameraName("birdeyes")
		expected := atoms.CameraName("birdeyes")
		if expected != b.CameraName {
			t.Errorf("SetCameraName set the wrong CameraName, set %v expected %v", b.CameraName, expected)
		}
	})
}

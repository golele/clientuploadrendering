package molecules

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/golele/clientuploadrenderings/src/atoms"
	mock "gitlab.com/golele/clientuploadrenderings/src/molecules/mocks"
)

func init() {
	atoms.Client = &mock.HTTPClient{}
}

func getTestJdataStruct(collectionID atoms.CollectionID) Jdata {
	assignments := []Assignment{
		{
			SymbolID: 1,
			ItemIds: []int{
				5323142,
				5332432,
			},
			ItemIDDefault: 5332432,
		},
		{
			SymbolID: 2,
			ItemIds: []int{
				6526537,
				5297510,
			},
			ItemIDDefault: 6526537,
		},
		{
			SymbolID: 4,
			ItemIds: []int{
				5342365,
				84291513,
			},
			ItemIDDefault: 5342365,
		},
		{
			SymbolID: 5,
			ItemIds: []int{
				5296108,
				84291511,
			},
			ItemIDDefault: 5296108,
		},
		{
			SymbolID: 8,
			ItemIds: []int{
				5332192,
			},
			ItemIDDefault: 5332192,
		},
		{
			SymbolID: 9,
			ItemIds: []int{
				5382866,
			},
			ItemIDDefault: 5382866,
		},
		{
			SymbolID: 12,
			ItemIds: []int{
				6302814,
			},
			ItemIDDefault: 6302814,
		},
		{
			SymbolID: 13,
			ItemIds: []int{
				6302814,
			},
			ItemIDDefault: 6302814,
		},
		{
			SymbolID: 14,
			ItemIds: []int{
				6131346,
			},
			ItemIDDefault: 6131346,
		},
	}

	return Jdata{Assignments: assignments, ID: collectionID}
}

func TestAPIBaseURL(t *testing.T) {
	// Given: the config file is valid
	//   And: the collectionID is set to 132
	//  Then: the API endpoint used to get the CollectionData should be ConfigOptions.BaseURL + "/collections/132"
	opts := ConfigOptions{
		BaseURL: "http://localhost/api/1/",
	}
	expected := "http://localhost/api/1/collections/132"
	t.Run("API endpoint", func(t *testing.T) {
		if got := getGETCollectionURL(opts, "132"); got != expected {
			t.Errorf("The API URL is wrong, got %s, expected %s", got, expected)
		}
	})
}

func TestHTTPHeader(t *testing.T) {
	// Given: the config file is valid
	//   And: the config.BasicAuth is set to "secret"
	req, _ := http.NewRequest("GET", "httpURL", nil)
	opts := ConfigOptions{
		BasicAuth: "secret",
	}
	addHeaderRequest(req, opts)
	expectedContentType := "application/json"
	//  Then: the http request should have the "Content-Type: application/json"
	t.Run("Content-Type", func(t *testing.T) {
		if got := req.Header.Get("Content-Type"); got != expectedContentType {
			t.Errorf("The http request should have 'Content-Type' %s, got %s", expectedContentType, got)
		}
	})
	expectedAuthorization := "Basic secret"
	//   And: the "Authorization: Basic secret" in his header
	t.Run("Authorization", func(t *testing.T) {
		if got := req.Header.Get("Authorization"); got != expectedAuthorization {
			t.Errorf("The http request should have 'Authorization' %s, got %s", expectedAuthorization, got)
		}
	})
}

func TestLoadData(t *testing.T) {
	// Given: the config file is valid
	// And: the collectionID is set to 132
	// When: the method collectionData.Load() have been called
	// Then: the json data loaded is saved in the struct Jdata
	json, _ := ioutil.ReadFile("./fixtures/collection132.json")
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	mock.GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}
	var collectionID atoms.CollectionID = 132
	expected := getTestJdataStruct(collectionID)
	opts := ConfigOptions{
		BaseURL: "http://vaseURL",
	}
	cData := CollectionData{}
	_ = cData.Load(opts, collectionID)
	if !reflect.DeepEqual(cData.Data, expected) {
		t.Errorf("TestLoadCollectionData expected %+v, got %+v", expected, cData.Data)
	}
}

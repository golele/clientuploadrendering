package mock

import "net/http"

// HTTPClient is the client mock
type HTTPClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

// GetDoFunc will be returned by Do
var GetDoFunc func(req *http.Request) (*http.Response, error)

// Do is the mock client's `Do` func
func (m *HTTPClient) Do(req *http.Request) (*http.Response, error) {
	return GetDoFunc(req)
}

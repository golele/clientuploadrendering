package molecules

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/golele/clientuploadrenderings/src/atoms"
)

// Assignment define a relation between SymbolID and ProductID (itemID)
type Assignment struct {
	SymbolID      atoms.SymbolID      `json:"symbol_id"`
	ItemIds       atoms.ItemIDs       `json:"item_ids"`
	ItemIDDefault atoms.ItemIDDefault `json:"item_id_default"`
}

// Jdata is the data that we use from one collectionData
type Jdata struct {
	Assignments []Assignment       `json:"assignments"`
	ID          atoms.CollectionID `json:"id"`
}

// CollectionData is the whole data returned by GET collections from the API
type CollectionData struct {
	Data Jdata `json:"data"`
}

func init() {
	atoms.Client = &http.Client{}
}

func getGETCollectionURL(opts ConfigOptions, collectionID string) string {
	return opts.BaseURL + "collections/" + collectionID
}

func addHeaderRequest(req *http.Request, opts ConfigOptions) {
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Basic "+opts.BasicAuth)
}

// Load the data of the collection with id collectionID
func (cdata *CollectionData) Load(opts ConfigOptions, collectionID atoms.CollectionID) error {
	req, err := http.NewRequest(http.MethodGet, getGETCollectionURL(opts, strconv.Itoa(int(collectionID))), nil)

	if err != nil {
		return err
	}
	addHeaderRequest(req, opts)
	resp, err := atoms.Client.Do(req)

	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {

		if resp.StatusCode == http.StatusNotFound {
			return fmt.Errorf("Collection Not Found")
		}
		return fmt.Errorf("Failed to Load Collection's Data. StatusCode: %d", resp.StatusCode)
	}

	if err = json.NewDecoder(resp.Body).Decode(&cdata); err != nil {
		return err
	}

	return nil
}

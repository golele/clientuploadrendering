package molecules

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"gitlab.com/golele/clientuploadrenderings/src/atoms"
)

// ConfigOptions are the options contained in the config file
type ConfigOptions struct {
	RenderedPhotoDirectory string `json:"rendered_photo_directory"`
	BaseURL                string `json:"base_url"`
	BasicAuth              string `json:"basic_auth"`
	ConfigFilePath         string
}

// Load the configuration's options from atoms.ConfigFile, if it exists
// return error if it doesn't exists
func (opts *ConfigOptions) Load() error {
	opts.ConfigFilePath = atoms.WorkingDir + "/config/" + atoms.ConfigFile + ".json"
	fileContent, err := ioutil.ReadFile(opts.ConfigFilePath)

	if err != nil {
		return err
	}

	err = json.Unmarshal(fileContent, opts)

	if err != nil {
		return err
	}
	return nil
}

// Check the configuration's options
func (opts *ConfigOptions) Check() error {
	if opts.BasicAuth == "" {
		return errors.New("basic_auth is not defined in the config file")
	}

	if opts.BaseURL == "" {
		return errors.New("base_url is not defined in the config file")
	}

	if opts.RenderedPhotoDirectory == "" {
		return errors.New("rendered_photo_path is not defined in the config file")
	}

	if opts.RenderedPhotoDirectory != "" {
		err := opts.updateRenderedPhotoDirectoryPath()

		if err != nil {
			return err
		}
	}
	return nil
}

func (opts *ConfigOptions) updateRenderedPhotoDirectoryPath() error {
	if _, err := os.Stat(opts.RenderedPhotoDirectory); err == nil {
		// The directory "rendered_photo_directory" specified in the config file have been found
		return nil
	}

	// Try to found the "rendered_photo_directory" in the current dir?
	searchPath := atoms.WorkingDir
	sep, _ := strconv.ParseUint(opts.RenderedPhotoDirectory[:1], 10, 32)

	if os.IsPathSeparator(uint8(sep)) {
		searchPath += opts.RenderedPhotoDirectory
	} else {
		searchPath += "/" + opts.RenderedPhotoDirectory
	}

	if _, err := os.Stat(searchPath); err == nil {
		opts.RenderedPhotoDirectory = searchPath
		return nil
	}
	msg := fmt.Sprintf("The directory of the rendered photo (%s) does not exists", searchPath)
	return errors.New(msg)

}

package atoms

import "net/http"

// CameraName is the name of the camera used in a renderd a photo
type CameraName string

// CollectionID is the ID of one Collection
type CollectionID int

// FileContent is the file's content of the renderedPhoto
type FileContent []byte

// ConfigFile is the configuration file for the options
var ConfigFile string

// HTTPClient define an interface
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client is of type HTTPClient
var Client HTTPClient

// initialize the client
func init() {
	Client = &http.Client{}
}

// ItemIDs is a lit of items
type ItemIDs []int

// ItemIDDefault is the default used in the list
type ItemIDDefault int

// LayoutID is the ID of one Layout. A Layout is the from of the (bed) Collection
type LayoutID uint32

// ProductID in the ID of one product that in present in one Combination
var ProductID uint32

// RenderedID is the ID of a rendered photo of one Combination (bed)
type RenderedID string

// RenderedPhotoPath is the directory containing the rendered photo to upload
var RenderedPhotoPath string

// SymbolID is the ID of one Symbol
type SymbolID int

// UploadAssignments is the association between the symbols and the items
// in one rendered photo
type UploadAssignments map[int]int

// WorkingDir is the current working directory, used to build the path for the config file
var WorkingDir string

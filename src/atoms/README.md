# Atoms

Atoms are the basic building blocks of all matter. Each chemical element has distinct properties, and they can’t be broken down further without losing their meaning. (Yes, it’s true atoms are composed of even smaller bits like protons, electrons, and neutrons, but atoms are the smallest functional unit.)

## List Of Atoms Presents In ClientUploadRenderings

- CollectionID
- FileContent
- ItemIDs
- LayoutID
- ProductID
- RenderedID
- SymbolID
- UploadAssignments
- WorkingDir

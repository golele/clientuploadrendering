# Client Upload Renderings

ClientUploadRenderings reads files from the rendered photos directory, passed as command line parameter with the flag "-i". This directory is a required parameter of the command line "clientUploadRenderins -h" and must have three levels of nested subdirectories.

The first level of the child's directory identify the collectionId
The second level identify the layoutId
The third (and last) level idenfity the CameraName
In the CameraName directory there are renderedPhotos
Example of the structure of the renderedPhotosDirectory

- the input Directory 160/1/garden/
  - 160 is the collectionId
  - 1 is the layoutId
  - garden is the CameraName

- image's file's name: 5322631_5670799_5332325_5574512_5459482_9497827_6302814_6302814_6131346.0001.png

`# clientUploadRenderings -i inputDirectory/`

In the last subdirectory (the "CameraName" directory) are saved the images that the clientUploadRenderings will load from the directory and sent through the PHP-API uploadRenderings from beetplaner.

The supported format for the images can be jpeg or png.

If a file is not an image nor a jpeg nor a png it will be skipped and it will be moved in a "failed" directory.

When one valid image is found, an http request will be sent through the API. The URL of the API can be passed as parameter of the command line (not-implemented yet).

The request must contain the following parameters:

- collectionId
- renderingId (the name of the image's file with collectionId and layoutId)
- image (the content of the image's file encoded in base64)
- layoutId
- cameraName
- assignments

The first two parameters are send als query paramenter in the URL of the API.
The input from the directory "renderedPhotosDirectory" and his structure give us most of the information that we need to build the request. But not the assignments. We have to get the assigments from the PHP-API

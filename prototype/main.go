package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// IDirectory command line parameter for the input directory
var IDirectory string

// OLogfile command line parameter to logs messages
var OLogfile string

// Sleeptime Set a pause, in seconds, between two successive uploads
var Sleeptime int

const SUCCESS_DIR = "successfullyUploaded/"
const FAILED_DIR = "failedToUpload/"
const BASE_HTACCESS_LOCAL = ""
const BASE_HTACCESS_DEV = ""
const BASE_HTACCESS_STAGE = ""
const BASE_HTACCESS_PROD = ""
const BASEURL_LOCAL = "http://localhost/api/1/collections/"
const BASEURL_DEV = ""
const BASEURL_STAGE = ""
const BASEURL_PROD = ""

// Assignment are the assignment of a collection
type Assignment struct {
	SymbolID      int   `json:"symbol_id"`
	ItemIds       []int `json:"item_ids"`
	ItemIDDefault int   `json:"item_id_default"`
}

type Jdata struct {
	Assignments []Assignment `json:"assignments"`
	ID          int          `json:"id"`
}

type CollectionData struct {
	Data Jdata `json:"data"`
}

type UploadAssignments map[int]int

type Rendering struct {
	id          string
	renderingID string
	Image       string            `json:"image"`
	LayoutID    int               `json:"layout_id"`
	CameraName  string            `json:"camera_name"`
	Assignments UploadAssignments `json:"assignments"`
}

func getBaseURL() string {
	return BASEURL_PROD
}

func addHeaderRequest(req *http.Request) {
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Basic "+BASE_HTACCESS_PROD)
}

func getGETUrl(collectionID string) string {
	url := getBaseURL() + collectionID

	return url
}

func (r *Rendering) getPUTUrl() (url string, err error) {
	if r.id == "" {
		err = fmt.Errorf("Failed to get the URL for the PUT request: missing collectionId")
	}

	if r.renderingID == "" {
		err = fmt.Errorf("Failed to get the URL for the PUT request: missing renderingID")
	}
	url = fmt.Sprintf("%s%s/renderings/%s", getBaseURL(), r.id, r.renderingID)

	return
}

func loadCollectionData(collectionID string) (jdata Jdata, err error) {
	req, err := http.NewRequest("GET", getGETUrl(collectionID), nil)

	if err != nil {
		return jdata, err
	}
	addHeaderRequest(req)
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		return jdata, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return jdata, fmt.Errorf("Failed to Load Collection's Data: %s", resp.Status)
	}
	cdata := new(CollectionData)

	if err = json.NewDecoder(resp.Body).Decode(&cdata); err != nil {
		return jdata, err
	}

	return cdata.Data, nil
}

// Set the assignment related to the renderingID
func (r *Rendering) setAssignments(jdata Jdata, filename string) error {
	name := strings.Split(filename, ".")
	items := strings.Split(name[0], "_")
	for i, v := range jdata.Assignments {
		for _, k := range v.ItemIds {
			item, err := strconv.Atoi(items[i])
			if err != nil {
				log.Fatal(err)
			}
			if item == k {
				r.Assignments[v.SymbolID] = item
				continue
			}
		}
	}

	return nil
}

func (r *Rendering) setLayoutID(layoutID string) {
	r.LayoutID, _ = strconv.Atoi(layoutID)
}

func (r *Rendering) setCameraName(cameraName string) {
	r.CameraName = cameraName
}

func (r *Rendering) setImagesData(filename string, imageName string) error {
	if r.id == "" {
		return fmt.Errorf("setImagesData: failed to build the renderingID, id (CollectionId) is missing")
	}

	if r.LayoutID == 0 {
		return fmt.Errorf("setImagesData: failed to build the renderingID, LayoutID is missing")
	}

	if r.CameraName == "" {
		return fmt.Errorf("setImagesData: failed to build the renderingID, CameraName is missing")
	}

	fileContent, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	// The files generated by the Layout tool are in the format
	// 5332432_7099344_5332325_5574512_5459482_9497827_6302814_6302814_6131346.0001.png
	// The renderingID don't need the last part .0001.png
	fileNameParts := strings.Split(imageName, ".")
	fileExtension := fileNameParts[len(fileNameParts)-1]
	r.renderingID = fmt.Sprintf("%d_%s_%s", r.LayoutID, r.CameraName, fileNameParts[0])

	// Save the base64 coded image's content in the r.Image
	r.Image = fmt.Sprintf("data:image/%s;base64,%s", fileExtension, base64.StdEncoding.EncodeToString((fileContent)))
	return nil
}

func (r *Rendering) resetImagesData() {
	r.renderingID = ""
	r.Image = ""
	r.Assignments = make(map[int]int)
}

func moveImageToFailedDir(relativeDir string, imageName string) {
	destDir := IDirectory + FAILED_DIR + relativeDir + "/" + imageName
	srcDir := IDirectory + relativeDir + "/" + imageName
	err := os.MkdirAll(IDirectory+FAILED_DIR+relativeDir, 0755)
	if err != nil {
		log.Fatal(err)
	}

	err = os.Rename(srcDir, destDir)
	if err != nil {
		log.Fatal(err)
	}
}

func moveImageToSuccessDir(relativeDir string, imageName string) {
	destDir := IDirectory + SUCCESS_DIR + relativeDir + "/" + imageName
	srcDir := IDirectory + relativeDir + "/" + imageName
	err := os.MkdirAll(IDirectory+SUCCESS_DIR+relativeDir, 0755)
	if err != nil {
		log.Fatal(err)
	}

	err = os.Rename(srcDir, destDir)
	if err != nil {
		log.Fatal(err)
	}
}

func (r *Rendering) upload() error {
	body, err := json.Marshal(&r)
	if err != nil {
		return fmt.Errorf("Failed to create JSON body %s", err)
	}
	url, err := r.getPUTUrl()

	if err != nil {
		return err
	}
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(body))

	if err != nil {
		return err
	}

	addHeaderRequest(req)
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Failed to Upload The Rendering %s, statusCode = %d", r.renderingID, resp.StatusCode)
	}
	return nil
}

func init() {
	flag.StringVar(&IDirectory, "i", "", "-i=inputdirectory")
	flag.StringVar(&OLogfile, "l", "stderror", "-l=logfile")
	flag.IntVar(&Sleeptime, "s", 0, "-s=sleeptime")
}

func main() {
	flag.Parse()

	files, err := ioutil.ReadDir(IDirectory)

	if err != nil {
		log.Fatal(err)
	}

	for _, l1 := range files {
		if l1.Name() == SUCCESS_DIR || l1.Name() == FAILED_DIR {
			continue
		}
		// Initialize Rendering's struct
		rendering := Rendering{id: l1.Name(), Assignments: make(map[int]int)}
		jdata, err := loadCollectionData(rendering.id)

		if err != nil {
			log.Println("Error: could not load the rendering's assignments!")
			log.Fatal(err)
		}
		dir := IDirectory + l1.Name()
		files, err = ioutil.ReadDir(dir)

		if err != nil {
			log.Fatal(err)
		}

		for _, l2 := range files {
			rendering.setLayoutID(l2.Name())
			dir := IDirectory + l1.Name() + "/" + l2.Name()
			files, err := ioutil.ReadDir(dir)

			if err != nil {
				log.Fatal(err)
			}

			for _, l3 := range files {
				rendering.setCameraName(l3.Name())

				// We have now the pathPrefix where the images are located
				relativeDdir := l1.Name() + "/" + l2.Name() + "/" + l3.Name()
				absDir := IDirectory + relativeDdir
				files, err := ioutil.ReadDir(absDir)

				if err != nil {
					log.Fatal(err)
				}

				for _, image := range files {
					imageName := image.Name()
					fileName := absDir + "/" + imageName
					rendering.setAssignments(jdata, imageName)

					if err := rendering.setImagesData(fileName, imageName); err != nil {
						log.Printf("Failed to load the file %s %s", fileName, err)
						moveImageToFailedDir(relativeDdir, imageName)
						continue
					}
					// Upload the file
					if err = rendering.upload(); err != nil {
						moveImageToFailedDir(relativeDdir, imageName)
						rendering.resetImagesData()
						log.Printf("Could not upload the image with name: %s\n", imageName)
						// TODO: add command line parameter to choose to continue or to break the process
						log.Fatal(err)
					}
					rendering.resetImagesData()
					moveImageToSuccessDir(relativeDdir, imageName)
					time.Sleep(time.Duration(Sleeptime) * time.Second)
				}
			}
		}
	}
}

package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/golele/clientuploadrenderings/src/atoms"
	"gitlab.com/golele/clientuploadrenderings/src/organisms"
)

func init() {
	flag.StringVar(&atoms.ConfigFile, "c", "", "-c=configFile")
}

var usageStr = `
	clientUploadRenderings -c configFile

	- configFile is the configuration file

	- The "rendered_photo_directory" specified in the config must contain one directory whose name is the name of the collectionID
	- the collection directory must contain one directory whose name is the name of the layoutID
	- the layout directory must contain one directory whose name is the name of the camera
	- ths camera directory should contain at least one rendered photos, otherwise you don't need it
`

// usage will print out the flag options for the server.
func usage() {
	fmt.Printf("%s\n", usageStr)
	os.Exit(0)
}

func checkFlags() {
	var strParams string

	if atoms.ConfigFile == "" {
		strParams += fmt.Sprint("Error: configFile is required\n")
	}

	if strParams != "" {
		fmt.Println(strParams)
		usage()
	}
}

func main() {
	flag.Parse()

	checkFlags()
	wd, err := os.Getwd()

	if err != nil {
		log.Fatal(err)
	}
	atoms.WorkingDir = wd
	if err := organisms.Run(); err != nil {
		log.Fatal(err)
	}
}
